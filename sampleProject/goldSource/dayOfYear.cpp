#include <iostream>
#include <iomanip>

using namespace std;

int numberOfDaysIn (int m, int y);
bool isValid (int y, int m, int d);
bool isLeap (int y);
void computeDayNumber(int y, int m, int d, int& dayOfYear);


int main()
{
  int year, month, day;
  char c;
  bool validInput = false;
  while ((cin) && !validInput)
  {
    cout << "Enter a date in the form YYYY-MM-DD: " << flush;
    cin >> year >> c >> month >> c >> day;

    // Check to see if this is a valid date

    validInput = isValid(year, month, day);

    if (!validInput)
      {
	cout << "Sorry, that is not a valid date" << endl;
	string garbage;
	getline (cin, garbage); // discard the rest of the input line
      }
  }
  if (!cin)
  {
    cout << "Could not obtain valid input." << endl;
    return -1;
  }

  // Input is valid - compute the day number
  int dayNum = 0;
  computeDayNumber(year, month, day, dayNum);

  cout << setw(2) << setfill('0') << month
       << "/" << setw(2) << setfill('0') << day << "/"
       << setw(4) << year;
  cout << " is day #" << dayNum << " of that year." << endl;

  return 0;
}


void computeDayNumber (int y, int m, int d, int& dayOfYear)
{
  // Add up the number of days in all earlier months
  // of this year
  dayOfYear = 0;
  for (int i = 1; i < m; i++)
    {
      dayOfYear += numberOfDaysIn(i, y);
    }

  // Then add the day number to that sum
  dayOfYear += d;
}

bool isValid (int y, int m, int d)
{
  // Check to see if this is a valid date
  // The Gregorian calendar began On Oct 15, 1582. Earlier dates
  // are invalid.
  if (y < 1582)
    return false;
  else if (y == 1582 && m < 10)
    return false;
  else if (y == 1582 && m == 10 && d < 15)
    return false;
  // Months must be in the range 1..12
  else if (m < 1 || m > 12)
    return false;
  // Days must be in the range 1..K where K is the number of
  // days in that month.
  else
  {
    int numberOfDaysInMonth = 0;
    numberOfDaysInMonth = numberOfDaysIn(m, y);
    if (d < 1 || d > numberOfDaysInMonth)
      return false;
    else
      return true;
  }
  cout << "Something went wrong in isValid" << endl;
  return false;
}

bool isLeap (int y)
{
  if ( ((y % 4 == 0)&&(y % 100 != 0)) || (y % 400 == 0) )
    return true;
  else
    return false;
}

int numberOfDaysIn (int m, int y)
{
  switch (m)
  {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      return 31;
    case 4:
    case 6:
    case 9:
    case 11:
      return 30;
    case 2:
      if (isLeap(y))
        return 29;
      else
        return 28;
    default:
      cout << "Something went wrong in numberOfDaysIn" << endl;
      return -1;
  }
}
