package edu.odu.cs.cs350.blue3;

import static org.junit.Assert.*;

import org.junit.Test;

public class OperatorSubTest extends OperatorSub {
	// Add test swaps 1 of 3
	 @Test
		 public void testAddSwap(){
		 String test = "x + y = z;";
		 String check = "x - y = z; // + operator swapped here";
		 String result = swapAddMinus(test);
		 assertEquals(check, result);
		 }
	 
	// Add test swaps 2 of 3
	 public void testAddMul(){
		 String test = "x + y = z;";
		 String check = "x * y = z; // + operator swapped here";
		 String result = swapAddMul(test);
		 assertEquals(check, result);
		 }
	 
	// Add test swaps 3 of 3
		 public void testAddDiv(){
			 String test = "x + y = z;";
			 String check = "x / y = z; // + operator swapped here";
			 String result = swapAddDiv(test);
			 assertEquals(check, result);
			 }
	 //Minus test swaps 1 of 3
		 @Test
		 public void testMinusSwap(){
		 String test = "x - y = z;";
		 String check = "x + y = z; // - operator swapped here";
		 String result = swapMinusAdd(test);
		 assertEquals(check, result);
		 }
}
