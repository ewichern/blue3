package edu.odu.cs.cs350.blue3;
import static org.junit.Assert.*;
import java.io.File;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class TestGetBuildFile extends GetBuildFile {
	GetBuildFile gbf = new GetBuildFile();
	@Before
	public void setUp() throws Exception {
		gbf = new GetBuildFile();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testgetFile() {
		File theFile = gbf.getBuildFile();
		assertEquals("makefile",theFile.getName());
	}

}
