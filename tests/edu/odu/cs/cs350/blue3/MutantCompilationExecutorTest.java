package edu.odu.cs.cs350.blue3;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.io.*;
import edu.odu.cs.cs350.blue3.MutantCompilationExecutor;

public class MutantCompilationExecutorTest {

	public MutantCompilationExecutor tester = new MutantCompilationExecutor();
	public File testDir = new File("sampleProject" 
									+ File.separator
									+ "buildScript"
									+ File.separator );
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}
	
	
	@Test
	public void testBuildWithMake() throws IOException {
		File expectedOutput = new File("sampleProject" + File.separator + "buildScript" + File.separator + "dayOfYear" );
		tester.buildWithMake(testDir);
		assertTrue(expectedOutput.exists());
	}
	
	/*
	@Test
	public void testFailedBuildWithMake() {
		fail("Not yet implemented");
	}
	*/
	
	@Test
	public void testPrintSummaryOfMutants() {
		tester.printSummaryOfMutants(testDir);
	}
	
	/*
	@Test
	public void testFailedPrintSummaryOfMutations() {
		fail("Not yet implemented");
	}
	*/
	
	@Test
	public void testDirectoryExists() {
		assertTrue(tester.directoryExists(testDir));
	}
	
	@Test
	public void testFailedDirectoryExists() {
		File failDir = new File ("sampleProject" 
									+ File.separator
									+ "buildScript"
									+ File.separator 
									+ "failDr");
		assertFalse(tester.directoryExists(failDir));
	}
	
}
