package edu.odu.cs.cs350.blue3;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

public class TestCompileSource {

	CompileSource gold;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		gold = new CompileSource();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCompileGoldSource() {
	}

	@Test
	public void testCompileGoldSourceString() {
	}

	@Test
	public void testCompile() throws Exception {
		
		gold.compile();
	}

}
