package edu.odu.cs.cs350.blue3;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import edu.odu.cs.cs350.blue3.InputTestSuite;

public class InputTestSuiteTest {
	
	public InputTestSuite tester = new InputTestSuite();
	public File testDir = new File("sampleProject" + File.separator + "testData" + File.separator );
	public String suffix = ".dat";
	
	@Before
	public void setUp() {
		
	}
	
	@After
	public void tearDown() {
		
	}
	
	@Test
	public void testDirectoryToList(){
		File[] testFileList = testDir.listFiles();
		
		List<String> testDataList = new ArrayList<String>();
		
		for (File file : testFileList) {
		    if (file.getName().endsWith((suffix))) {
		      testDataList.add(file.getName());
		    }
		  }
		
		List<String> actualList = new ArrayList<String>();
		actualList = tester.DirectoryToList(testDir, suffix);
		
		assertEquals(testDataList, actualList);
	}
	
	@Test
	public void failDirectoryToList(){
		File[] testFileList = testDir.listFiles();
		List<String> testDataList = new ArrayList<String>();
		String wrongSuffix = ".exe";
		
		for (File file : testFileList) {
		    if (file.getName().endsWith((wrongSuffix))) {
		      testDataList.add(file.getName());
		    }
		  }
		
		List<String> actualList = new ArrayList<String>();
		actualList = tester.DirectoryToList(testDir, suffix);
		
		assertNotEquals(testDataList, actualList);
	}
}
