package edu.odu.cs.cs350.blue3;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Test;

import java.io.IOException;
import java.io.File;

/**
 * Junit tests for CompareScripts class
 * 
 * @author Erik Wichern - ewichern@cs.odu.edu
 */
public class TestCompareScripts {

	private CompareScripts scripts;
	private static int testCount=0;
	private CompareScripts scriptsNoFileRead;
	private static String sourceDir = "./sampleProject/";
	/*
	 * private static String testDir = "./testingProject/";
	 */
	
	/**
	 * Set up once before all tests
	 * No parameters, no return
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/*
		ProcessBuilder copy = new ProcessBuilder("cp", "-r", sourceDir, testDir);
		copy.start();
		System.err.println("cp -r "+sourceDir+" "+testDir);
		*/
		
		File workingSampleDir = new File(sourceDir);
		ProcessBuilder sampleClean = new ProcessBuilder("make", "-f",
				sourceDir+"buildScript/makefile", "clean");
		sampleClean.directory(workingSampleDir);
		sampleClean.start();

		ProcessBuilder sampleMake = new ProcessBuilder("make", "-f",
				sourceDir+"buildScript/makefile");
		sampleMake.directory(workingSampleDir);
		sampleMake.start();
		
		/*
		File workingTestDir = new File(testDir);
		ProcessBuilder testClean = new ProcessBuilder("make", "-f",
				testDir+"buildScript/makefile", "clean");
		testClean.directory(workingTestDir);
		testClean.start();

		ProcessBuilder testMake = new ProcessBuilder("make", "-f",
				testDir+"buildScript/makefile");
		testMake.directory(workingTestDir);
		testMake.start();
		*/

	}

	/**
	 * Clean up once after all tests
	 * No parameters, no return
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		File workingSampleDir = new File(sourceDir);
		ProcessBuilder sampleClean = new ProcessBuilder("make", "-f",
				sourceDir+"buildScript/makefile", "clean");
		sampleClean.directory(workingSampleDir);
		sampleClean.start();
		
		/*	
	    ProcessBuilder rm = new ProcessBuilder("rm", "-rf", testDir);
		rm.start();
		*/
	}
	
	/**
	 * Set up before each test
	 * No parameters, no return
	 */
	@Before
	public void setUp() throws Exception {
		testCount++;
		System.err.println("Test Number: " + testCount);
		scripts = new CompareScripts();
		
		File workingDir = new File(sourceDir);
		ProcessBuilder q = new ProcessBuilder("make", "-f",
				sourceDir+"buildScript/makefile", "cleanTests");
		q.directory(workingDir);
		q.start();

		scriptsNoFileRead = new CompareScripts();
	}

	/**
	 *  Clean up after each test
	 *  No parameters, no return
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test the default constructor
	 * Ensure that it is not null after creation.
	 * No parameters, no return
	 */
	@Test
	public void testCompareScriptsDefault() {
		CompareScripts test1 = new CompareScripts();
		assertNotNull("Object created, should not be null.", test1);
	}
	
	/**
	 * Test constructor
	 * Accepts directory name as a string parameter. Ensure that it is not 
	 * null after creation.
	 * No parameters, no return
	 */
	@Test
	public void testCompareScriptsDirectoryConstructor() throws Exception {
		CompareScripts testConstructor = new CompareScripts(sourceDir);
		assertNotNull("Object created, should not be null.", testConstructor);
		assertEquals("List should be empty right now.", true, testConstructor.isEmpty());
		
		testConstructor.getFiles();
		System.err.println(testConstructor.isEmpty());
		assertEquals("List should not be empty", false, testConstructor.isEmpty());
		
		testConstructor.runScripts();
	}

	/**
	 * Test the isEmpty() function 
	 * Checks an empty object and a non-empty object
	 * No parameters, no return
	 */
	@Test
	public void testIsEmpty() throws Exception {
		assertTrue(scriptsNoFileRead.isEmpty());
		scripts.getFiles();
		assertEquals("List should not be empty.", false, scripts.isEmpty());
	}

	@Test
	public void testGetFiles() throws Exception {
		scripts.getFiles();
	}
	
	/**
	 * Test the runScripts() function
	 * Runs the function, should not throw an exception
	 */
	@Test
	public void testRunScripts() throws Exception {
		scripts.getFiles();
		scripts.runScripts();
	}

}
