package edu.odu.cs.cs350.blue3;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.nio.file.*;

public class TestMutantRunner {

	MutantRunner mutants;
	File goldFile = new File("./sampleProject/goldSource/dayOfYear.cpp");
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMutantTest() throws Exception {
		mutants = new MutantRunner ("./sampleProject/goldSource/dayOfYear.cpp");
		
		MutationOperator.deleteStatement(MutationOperator.LoadSourceFile(goldFile), goldFile); 
		mutants.testMutants();
		System.err.println("Total Mutants: " + mutants.getTotalMutants());
		System.err.println("Killed Mutants: " + mutants.getNumKilledMutants());
		System.err.println("LiveMutants: " + mutants.getNumLiveMutants());
	}

}
