package edu.odu.cs.cs350.blue3;

import java.io.File;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class InputGoldSourceTest extends InputGoldSource {
	private InputGoldSource gs= new InputGoldSource();
	private File rootDir = new File("./testProj");
	private File theDir = new File("./testProj/goldSource/");
	private String fileName1 ="test";
	private File file1 = new File(theDir,fileName1+".cpp");
	private File file2 = new File(theDir,fileName1+".h");
	private File theSubDir = new File("./testProj/goldSource/Class1");
	private File file3 = new File(theSubDir,fileName1+".java");
	private File theSubDir2 = new File("./testProj/goldSource/Class2");
	private File file4 = new File(theSubDir2,fileName1+".java");
	@Before
	public void setUp() throws Exception {
		//Create goldSource directories if they do not exist
		rootDir.mkdir();
		theDir.mkdir();
		theSubDir.mkdir();
		theSubDir2.mkdir();
		  
		//Add two C++ files and two Java files to the ./gourSource directory
		if(!file1.exists()){
			file1.createNewFile();
		}
		if(!file2.exists()){
			file2.createNewFile();
		}
		if(!file3.exists()){
			file3.createNewFile();
		}
		if(!file4.exists()){
			file4.createNewFile();
		}
		
	}
	
	@After
	public void tearDown(){
		//String fileName1 ="test";
		//File theDir = new File("./goldSource/");
		//File file1 = new File(theDir,fileName1+".cpp");
		//File file2 = new File(theDir,fileName1+".h");
		 file1.delete();
		 file2.delete();
		 file3.delete();
		 file4.delete();
		 
		 theSubDir.delete();
		 theSubDir2.delete();
		 theDir.delete();
		 rootDir.delete();
	}
	
	@Test
	public void testgetFiles() {
		//assertEquals(1,1);
		gs.getFiles("./testProj");
		assertEquals("Number of files in directory vs. number of files in source file array",4,gs.getSource().size());
	}

}
