package edu.odu.cs.cs350.blue3;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MutationOperatorTest extends MutationOperator {
	File fileName = new File("OpsSub.java");
	private File theDir = new File("./tests");

	@Before
	public void setUp() throws Exception {
		// Load source code into an Array List
	}

	@After
	public void tearDown() throws IOException {
		List<String> source = LoadSourceFile(fileName);
		source.clear();
		;
	}

	@Test
	public void testLoadSourceFile() throws IOException {
		List<String> source = LoadSourceFile(fileName);
		// assert the file was loaded into the list
		assert (source.size() != 0);
	}

	@Test
	public void testCreateMutationFile() throws IOException {
		List<String> source = LoadSourceFile(fileName);
		int counter = 1;
		String find = "+";
		swapOperator(source, "+", "-", fileName);
		createMutationFile(source, counter, find, fileName);
		assertEquals(1, theDir.list().length);

	}

	@Test
	public void swapOperator() throws IOException {
		List<String> source = LoadSourceFile(fileName);
		String find = "+";
		String replace = "-";
		String contains = "z - x";
		File expectedName = new File("sampleProject/mutations/1mutation+.java");
		swapOperator(source, find, replace, fileName);
		assertTrue (expectedName.exists());
	}
	
	@Test
	public void deleteStatement() throws FileNotFoundException, IOException {
		List<String> source = LoadSourceFile(fileName);
		File expectedName = new File("sampleProject/mutations/1mutationDS.java");
		deleteStatement(source, fileName);
		assertTrue (expectedName.exists());
	}
}
