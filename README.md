## blue3 - Mutation Testing Project

### Description

[Project - Outline/Description](https://drive.google.com/file/d/1EQTXImwF8ysKW5Rc-My3_8BuP6HNA3mUl23tlgBKgcgvQm50V-hLaIrSCAZgdODg8tREJcliZGMSmeWC/view?usp=sharing)

This was a group project intended to teach students elements of the SDLC and tools and techniques required to work in a collaborative professional environment. Deliverables included a Software Requirements Specification (SRS), User Stories, and then submissions of our code at the end of regular "sprints" for review. User stories were chose for implementation for each sprint and gitlab was used for version control and issue tracking. 

Note that we did deliver several milestone increments, but the code here is not in any way finished or intended to be used as a working product. Specifically, there's a performance problem that I never got around to isolating and fixing. So run/use anything here at your own risk!

##### Course topics included:

- Software Development Life Cycle (SDLC)
- requirements writing
- IDE setup and use
- Verification and Validation (V&V)
- tests and mocks
- Google Test
- JUnit
- test automation
- regression testing
- system testing
- build management (make, ant, maven)
- dependency management
- configuration management (maven or ivy)
- version control (both centralized and distributed, i.e. cvs vs. git)
- documentation with Javadoc
- documentation generators in other languages
- test coverage
- static and dynamic program analysis
- continuous integration (CI)
- Agile, Extreme Programming (XP), Scrum

### Setting up 

Run `ant retrieve-ivy` at least once on your local machine to make sure you have the proper libraries in your classpath for development. Otherwise tests aren't going to compile properly!

Assuming that's been done, `ant build` is the standard command.

To clean up, `ant clean` or `ant cleaner`.

### sampleProject/ directory structure

Our sample and/or testing project is included. Following are directory descriptions. Design was intended to allow replacing files in these directories -- with potentially arbitrary numbers of alternative source/test files -- as long as the directory structure is adhered to.

- buildScript/ - should contain a make buildfile
- goldSource/ - source code for the gold version of the program to be tested against
- executeScripts/ - scripts to execute tests and capture output
- compareScripts/ - scripts to compare test output
- testData/ - input data for tests, also used in our example as location to save the "expected" output from the gold version to compare against.

### Group Members
- Erik Wichern
- Matthew Breden
- Anthony Baron
- Timothy Holmes