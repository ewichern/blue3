package edu.odu.cs.cs350.blue3;

import java.lang.Runtime;
import java.io.*;
import java.io.File;

/**
 * Class designed to take File directory as input 
 * and execute the "make" command on both C++ and Java files.
 * C++ input will yield *.exe, Java input will yield *.jar.
 * 
 * Method for determining if a directory exists. 
 * Method for determining how many mutants were successfully compiled. 
 * 
 * @author tholmes
 * @version 1.1
 * @since 2014-10
 */

public class MutantCompilationExecutor {
	/*
	 * Variable that stores how many mutations are successfully compiled
	 */
	static int summaryOfTotalMutants = 0; 
	
	//File dirToBuildScript = new File("codeToTest" + File.separator + "HelloWorldTestC" + File.separator);

	/**
	 * Method that builds the project using a make file.
	 * <p>
	 * 
	 * @param dirToBuildScript is a File path to where the build script is located.
	 */
	public void buildWithMake (File dirToBuildScript) throws IOException {
		if (directoryExists(dirToBuildScript)) {			
			String command = "make";
			String[] envp = { };
			Runtime.getRuntime().exec(command, envp, dirToBuildScript);
			summaryOfTotalMutants++;
		} else {
			System.err.println("Directory does not exist");
		}
	}
	
	
	/**
	 * Method that provides a summary of total mutants and .exe files yielded from those mutants.
	 * <p>
	 * Method that checks how many .exe files are in a particular directory. Then, prints out how many mutants 
	 * were ran through the "buildWithMake" method, along with how many mutants compiled successfully, 
	 * and resulted in a ".exe" file.
	 * <p>
	 * @param dirToExeFiles is the directory that houses the .exe files to be counted.
	 * 
	 */
	public void printSummaryOfMutants(File dirToExeFiles) {
		int count = 0;
		String suffix = ".exe";
		
		for (File file : dirToExeFiles.listFiles()) {
			if (file.isFile() && file.getName().endsWith(suffix)) {
				count++;
			}
		}
		
		System.out.println("Attempted to compile " + summaryOfTotalMutants + " files.");
		System.out.println(count + " mutants succesfully compiled as a .exe file.");
	}
	
	/**
	 * Confirms or denies the existence of a particular directory 
	 * <p>
	 * @returns Files.exists();
	 */
	public boolean directoryExists(File dir) {
		if (dir.exists())
			return true;
		else
			return false;
	}
}