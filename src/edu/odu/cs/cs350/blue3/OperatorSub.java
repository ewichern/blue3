package edu.odu.cs.cs350.blue3;



public class OperatorSub {

	/**
	 * Arithmetic operator substitution methods. +, -, /, and * are all swapped
	 * with each of the other operators available
	 **/
	// Add swaps 1 of 3
	public static String swapAddMinus(String s) {
		String newS = "";
		if (s.contains("+")) {
			newS = s.replace('+', '-');
		}
		return newS + " // + operator swapped here";
	}

	// Add swaps 2 of 3
	public static String swapAddMul(String s) {
		String newS = "";
		if (s.contains("+")) {
			newS = s.replace('+', '*');
		}
		return newS + " //+ operator swapped here";
	}

	// Add swaps 3 of 3
	public static String swapAddDiv(String s) {
		String newS = "";
		if (s.contains("+")) {
			newS = s.replace('+', '/');
		}
		return newS + " //+ operator swapped here";
	}

	// Minus swaps 1 of 3
	public static String swapMinusAdd(String s) {
		String newS = "";
		if (s.contains("-")) {
			newS = s.replace('-', '+');
		}
		return newS + " // - operator swapped here";
	}

	// Minus swaps 2 of 3
	public static String swapMinusMul(String s) {
		String newS = "";
		if (s.contains("-")) {
			newS = s.replace('-', '*');
		}
		return newS + " // - operator swapped here";
	}

	// Minus swaps 3 of 3
	public static String swapMinusDiv(String s) {
		String newS = "";
		if (s.contains("-")) {
			newS = s.replace('-', '/');
		}
		return newS + " // - operator swapped here";
	}

	// Divide swaps 1 of 3
	public static String swapDivideAdd(String s) {

		String newS = "";
		if (s.contains("/")) {
			newS = s.replaceFirst("/", "+");
		}

		return newS + " // '/' operator swapped here";
	}

	// Divide swaps 2 of 3
	public static String swapDivideMinus(String s) {

		String newS = "";
		if (s.contains("/")) {
			newS = s.replaceFirst("/", "-");
		}

		return newS + " // '/' operator swapped here";
	}

	// Divide swaps 3 of 3
	public static String swapDivideMul(String s) {

		String newS = "";
		if (s.contains("/")) {
			newS = s.replaceFirst("/", "*");
		}

		return newS + " // '/' operator swapped here";
	}

	// Multiply swaps 1 of 3
	public static String swapMultiplyAdd(String s) {
		String newS = "";
		if (s.contains("*")) {
			newS = s.replaceFirst("\\*", "+");
		}
		return newS + " // * operator swapped here";
	}

	// Multiply swaps 2 of 3
	public static String swapMultiplyMinus(String s) {
		String newS = "";
		if (s.contains("*")) {
			newS = s.replaceFirst("\\*", "-");
		}
		return newS + " // * operator swapped here";
	}

	// Multiply swaps 3 of 3
	public static String swapMultiplyDiv(String s) {
		String newS = "";
		if (s.contains("*")) {
			newS = s.replaceFirst("\\*", "/");
		}
		return newS + " // * operator swapped here";
	}

	/**
	 * Relational operator swap methods. Replaces relational operator with other
	 * available relational operators
	 **/
	// Less than swaps 1 of 3
	public static String swaplessToLessEqual(String s) {
		String newS = "";
		if (s.contains("<")) {
			newS = s.replace("<", "<=");
		}
		return newS + " // < operator swapped here";
	}

	// Less than swaps 2 of 3
	public static String swaplessToGreat(String s) {
		String newS = "";
		if (s.contains("<")) {
			newS = s.replace("<", ">");
		}
		return newS + " // < operator swapped here";
	}
	// Less than swaps 3 of 3
		public static String swaplessToEqual(String s) {
			String newS = "";
			if (s.contains("<")) {
				newS = s.replace("<", "==");
			}
			return newS + " // < operator swapped here";
		}

	// @Test
	// public void testAddSwap(){
	// String test = "x + y = z;";
	// String check = "x - y = z; //operator swapped here";
	// String result = swapAdd(test);
	// assertEquals(check, result);
	// }
	//
	// @Test
	// public void testMinusSwap(){
	// String test = "x - y = z;";
	// String check = "x + y = z; //operator swapped here";
	// String result = swapMinus(test);
	// assertEquals(check, result);
	// }
	// @SuppressWarnings("deprecation")
//	public static void main(String[] args) {
//
//		String test = "x + y = z;";
//		String test2 = "x - y = z;";
//		String test3 = "x / y = z; ///";
//		String test4 = "x * y = z; /**comment block test**/";
//		System.out.println(swapAddMinus(test));
//		System.out.println(swapAddMul(test));
//		System.out.println(swapAddDiv(test));
//		System.out.println(swapMinusAdd(test2));
//		System.out.println(swapMinusMul(test2));
//		System.out.println(swapMinusDiv(test2));
//		System.out.println(swapDivideAdd(test3));
//		System.out.println(swapDivideMinus(test3));
//		System.out.println(swapDivideMul(test3));
//		System.out.println(swapMultiplyAdd(test4));
//		System.out.println(swapMultiplyMinus(test4));
//		System.out.println(swapMultiplyDiv(test4));
//
//	}
}
