package edu.odu.cs.cs350.blue3;

import java.io.File;
import java.nio.file.*;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

public class MutantRunner {

	MutantRunner(String goldLoc) {
		projectDir = new File(projectDefault);
		mutantDir = new File(projectDefault + "mutations/");
		goldFile = new File(goldLoc);
		goldBackup = new File(goldFile.getPath() + ".bak");
		killedMutants = 0;
		liveMutants = 0;
	}

	MutantRunner(String projectLoc, String goldLoc) {
		projectDir = new File(projectLoc);
		mutantDir = new File(projectLoc + "mutations/");
		goldFile = new File(goldLoc);
		goldBackup = new File(goldFile.getPath() + ".bak");
		killedMutants = 0;
		liveMutants = 0;
	}

	private void saveGoldVersion() {
		try {
			Files.copy(goldFile.toPath(), goldBackup.toPath(),
					java.nio.file.StandardCopyOption.REPLACE_EXISTING,
					java.nio.file.StandardCopyOption.COPY_ATTRIBUTES);
		} catch (IOException e) {
			System.err
					.println("Something went wrong copying the gold source to backup.");
		}
	}

	private void replaceGoldVersion(File newSource) {
		try {
			Files.copy(newSource.toPath(), goldFile.toPath(),
					java.nio.file.StandardCopyOption.REPLACE_EXISTING,
					java.nio.file.StandardCopyOption.COPY_ATTRIBUTES);
		} catch (IOException e) {
			System.err
					.println("Something went wrong copying the gold backup back to the original location.");
		}
	}

	private void overWriteFileB(File a, File b) {
		try {
			Files.copy(a.toPath(), b.toPath(),
					java.nio.file.StandardCopyOption.REPLACE_EXISTING,
					java.nio.file.StandardCopyOption.COPY_ATTRIBUTES);
		} catch (IOException e) {
			System.err
					.println("Something went wrong copying the gold backup back to the original location.");
		}
	}

	public void testMutants() throws Exception {
		saveGoldVersion();
		File[] mutantFiles = mutantDir.listFiles();
		totalMutants = mutantFiles.length;
		
		for (int fileNumber = 0; fileNumber < totalMutants; fileNumber += 1) {
			replaceGoldVersion(mutantFiles[fileNumber]);

			CompileSource compileObject = new CompileSource(
					projectDir.getPath());

			try {
				compileObject.compile();
			} catch (Exception e) {
				System.err.println("Did not work, kill!");
				killedMutants += 1;
				try {
					Files.delete(mutantFiles[fileNumber].toPath());
				} catch (IOException io) {
					System.err
							.println("Problem deleting a mutant that should be killed!");
				}
			}

			ExecuteScripts executeAndCapture = new ExecuteScripts(
					projectDir.getPath());

			try {
				executeAndCapture.getFiles();
				executeAndCapture.runScripts();
			} catch (Exception e) {
				System.err.println("Did not work, kill!");
				killedMutants += 1;
				try {
					Files.delete(mutantFiles[fileNumber].toPath());
				} catch (IOException io) {
					System.err
							.println("Problem deleting a mutant that should be killed!");
				}
			}
			
			File compareOutput = new File(projectDir.getPath()
					+ "/test-reports/scriptCompareOutput.txt");
			
			overWriteFileB(blankFile, compareOutput);

			CompareScripts compareToExpected = new CompareScripts(
					projectDir.getPath());

			try {
				compareToExpected.getFiles();
				compareToExpected.runScripts();
			} catch (Exception e) {
				System.err.println("Odd, problem with the comparison scripts?");
			}

			
			FileReader readOutput = new FileReader(compareOutput);
			BufferedReader outputBuffer = new BufferedReader(readOutput);
			/* String bufferString = outputBuffer.readLine();
			 * System.err.println(bufferString);
			 */
			System.err.println(outputBuffer.ready());
			System.err.println(compareOutput.length());
			long test = 0;
			
			if ( !(compareOutput.length() == test) ) {
				killedMutants += 1;
				try {
					Files.delete(mutantFiles[fileNumber].toPath());
				} catch (IOException e) {
					System.err
							.println("Problem deleting a mutant that should be killed!");
				}
			} else {
				liveMutants += 1;
			}
			outputBuffer.close();
		}
		replaceGoldVersion(goldBackup);
	}

	public int getNumKilledMutants() {
		return killedMutants;
	}

	public int getNumLiveMutants() {
		return liveMutants;
	}

	public int getTotalMutants() {
		return totalMutants;
	}

	private int killedMutants;
	private int liveMutants;
	private int totalMutants;

	private String projectDefault = "./sampleProject/";
	private File blankFile = new File(projectDefault + "blankfile.txt");

	private File projectDir;
	private File mutantDir;
	private File goldFile;
	private File goldBackup;
}
