package edu.odu.cs.cs350.blue3;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class  inputmanager implements Cloneable{
	//Establishes file variable for importing all files
	
	//Define Directory
	public  String directory;
	
    static String temp = "";
    
	//New Holder
	public inputmanager()
	{
		myVector = new Vector<String>(1,1);
		fileVector = new Vector<Vector>(1,1);
	}
	
	public  String getDirectory()
	{
		Scanner in = new Scanner(System.in);
		String s;
		System.out.println("Input Directory under absolute path name");
		s = in.nextLine();
		directory = s;
		return directory;
	}
	
	
	  //Will later develop into class for OOP usage
	  public void getFiles() {
		 File folder = new File(directory);  
		 // TODO Auto-generated method stub
	    System.out.println("Reading files under the folder "+ folder.getAbsolutePath());
        //Keeps track of file count
	    int i = 0;
        
        //Vector of the string inputed from code
        //Vector<String> myVector = new Vector<String>(1,1);
        //Vector of vectors which contain strings
        //Vector<Vector> fileVector = new Vector<Vector>(1,1);
        
	    for (final File fileEntry : folder.listFiles())
	    {
	        if (fileEntry.isFile()) 
	        {
	          //Logic for sorting of necessary files, will be changed in later implementation
	          temp = fileEntry.getName();
	          if ((temp.substring(temp.lastIndexOf('.') + 1, temp.length()).toLowerCase()).equals("txt") || 
	        	  (temp.substring(temp.lastIndexOf('.') + 1, temp.length()).toLowerCase()).equals("cpp") ||
	        	  (temp.substring(temp.lastIndexOf('.') + 1, temp.length()).toLowerCase()).equals("java")||
	        	  (temp.substring(temp.lastIndexOf('.') + 1, temp.length()).toLowerCase()).equals("h"))
	          {
	        	//Output exist for testing program
	        	System.out.println("File= " + folder.getAbsolutePath()+ "\\" + fileEntry.getName());
	            //Increments file count,  can be removed after development pahase
	        	i++;
	        	//Adds 
	            myVector.add(temp);
	          
	            
	            Vector<String> v = new Vector<String>(1,1);
	            String inputLine;
	            try {
	              File inFile = new File(folder.getAbsolutePath()+ "\\" + temp);
	              BufferedReader br = new BufferedReader(new InputStreamReader(
	                  new FileInputStream(inFile)));

	              while ((inputLine = br.readLine()) != null) {
	                v.addElement(inputLine.trim());
	              }
	              br.close();
	            } // Try
	            catch (FileNotFoundException ex) {
	              //
	            } catch (IOException ex) {
	              //
	            }      
	          
	    	   
	    	    //System.out.println("File #= " + i);
		        //System.out.println(temp);
	            fileVector.add(v);
	           
	          }
	        }
	        
	    }
	    
	    //Output directly from 2d vector
	    for ( int j = 0; j < fileVector.capacity(); j++ )
	    {
	   	   for(int k = 0; k < fileVector.elementAt(j).capacity() ;k++)
	   	   {   
	    	
	    	System.out.println(fileVector.elementAt(j).elementAt(k));
	   	   }
	   	System.out.println("");
	    }	    
	} 
	  
	public Vector getVector(int i)
	{
      return fileVector.elementAt(i);
			
	}
	  private Vector<String> myVector;
	  private Vector<Vector> fileVector;
	  
}