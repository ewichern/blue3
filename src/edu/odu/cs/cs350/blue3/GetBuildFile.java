package edu.odu.cs.cs350.blue3;

import java.io.File;
import java.util.ArrayList;


public class GetBuildFile {
	
	/**
	 * Default constructor, no input or output.
	 * Does nothing, but we have a couple private data members
	 * that get initialized.
	 */
	public GetBuildFile()
	{
		BuildDir = new File(projectDirName+buildDirName);
	}
	
	public GetBuildFile(String projectDir) 
	{
		BuildDir = new File(projectDir+"/"+buildDirName);
	}
	
	public GetBuildFile(File projectDirFile)
	{
		BuildDir = new File(projectDirFile.getPath()+"/"+buildDirName);
	}
	
		
	/**
	 * Reads in file from source directory 
	 * specified at the top of the class
	 * No input or output
	 */
	public File readDir()
	{
		if (BuildDir.exists() && BuildDir.isDirectory())
		{
			/* the build script should be the only file in the directory. 
			 * So we want to get the first file in the directory.
			 */
			//assign everything in the directory to an array (listing)
			
			for (File eachFile : BuildDir.listFiles())
			{
				if (eachFile.isFile())
				{
					return eachFile;
				}
				
			}		
			
			System.err.println("No build file was found.");
			return BuildDir;
			
		
			
		}
		System.err.println("Directory for build script (buildScript) does not exist.");
		return BuildDir;
	}
	
	
	/**
	 * Echo name of build file to screen.
	 */
	public void echoBuildFile()
	{
		System.out.println("Build File is: "+ getName());
	}
	
	public File getFile() {
		return buildFile;
	}
	
	public File getBuildFile() {
		buildFile = readDir();
		return buildFile;
	}

	public void setbuildFile(File buildFile) {
		this.buildFile = buildFile;
	}

	public String getName() {
		return buildFile.getName();
	}

	/**
	 * String listing the path to the project with subdirectories 
	 * containing scripts that describe test execution and output comparison.
	 * Default is "./sampleProject/"
	 */
	private String projectDirName = "./sampleProject/";
	/**
	 * String specifying the subdirectory of the project where script 
	 * describing test execution resides - "executeScripts/" 
	 */
	private String buildDirName = "buildScript/";
	/**
	 * String combining project location with execution script location
	 * Should be initialized by constructor
	 */
	
	private File BuildDir;
	private File buildFile;
}
