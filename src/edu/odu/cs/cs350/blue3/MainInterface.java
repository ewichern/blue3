package edu.odu.cs.cs350.blue3;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.*;

/**
 * Class for implementing the GUI.
 * 
 * Variables for design components.
 * Method for setting the layout and arranging the components of GUI.
 * Method to define the look and feel of GUI.
 * Method to set JCheckBox values.
 * Method to list files in a given directory.
 * Method to respond to user interaction with UI components.
 * 
 * @author tholmes
 * @version: 1.2
 * @since: 2014-11-20
 *
 */

@SuppressWarnings("serial")
public class MainInterface extends JFrame implements ActionListener {

	//Row 1
	JPanel row1 = new JPanel();
	JLabel pathLabelToCode = new JLabel("Path to Source Code: ", JLabel.RIGHT);
	JTextField pathAddressToCode = new JTextField("./sampleProject/goldSource/", 25);
	
	//Row 2
	JPanel row2 = new JPanel();
	JLabel filesToMutate = new JLabel("Files available to mutate: ");

	//Row2 Sub Panels
	CardLayout cl = new CardLayout();
	JPanel subPanelWithCards = new JPanel(cl);
	JPanel subPanelWithLabel = new JPanel();
	JPanel subPanelWithButton = new JPanel();
	ArrayList<JCheckBox> files = new ArrayList<JCheckBox>();
	
	//Row 3
	JPanel row3 = new JPanel();
	JLabel pathLabelToCompScript = new JLabel("Path to Script for Output Comparison: ", JLabel.RIGHT);
	JTextField pathAddressToCompScript = new JTextField("./sampleProject/compareScripts/", 25);
	
	

	/**
	 * Method for setting the layout and arranging the components of GUI.
	 * <p>
	 * 
	 * @param none
	 * @return none
	 */
	public MainInterface() {
		super("MainInterface");
		setLookAndFeel();
		setSize(750, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		GridLayout main = new GridLayout(3,1);
		setLayout(main);
		
		JButton enter = new JButton("enter");
		enter.addActionListener(this);
		enter.setActionCommand("enterCodePath");
		
		JButton generate = new JButton("Generate Mutants");
		generate.addActionListener(this);
		generate.setActionCommand("genMut");
		
		JButton enter2 = new JButton("enter");
		enter2.addActionListener(this);
		enter2.setActionCommand("enterScriptPath");
		
		FlowLayout firstRow = new FlowLayout(FlowLayout.CENTER);
		row1.setLayout(firstRow);
		row1.add(pathLabelToCode);
		row1.add(pathAddressToCode);
		row1.add(enter);
		add(row1);
		
		
		GridLayout secRow = new GridLayout(1,3);
		row2.setLayout(secRow);
		subPanelWithLabel.add(filesToMutate);
		subPanelWithCards.add(new JLabel("No Files to Show", SwingConstants.CENTER), "empty");
		cl.show(subPanelWithCards, "empty");
		subPanelWithButton.add(generate);
		row2.add(subPanelWithLabel);
		row2.add(subPanelWithCards);
		row2.add(subPanelWithButton);
		add(row2);
		
		FlowLayout thirdRow = new FlowLayout(FlowLayout.CENTER);
		row3.setLayout(thirdRow);
		row3.add(pathLabelToCompScript);
		row3.add(pathAddressToCompScript);
		row3.add(enter2);
		add(row3);
		
		setVisible(true);
	}
	
	/**
	 * Method that populates a JCheckBox List with files in a directory.
	 * <p>
	 * This method creates the interface for the user to select files to mutate. It takes 
	 * a directory, adds the file names to a JCheckBox group, and then adds them to the panel.
	 * 
	 * @param dirToSrcCode
	 * @return JPanel panel, 
	 */
	public JPanel setCheckBox(File dirToSrcCode) {
		JPanel panel = new JPanel();
		
		File[] listOfSrcCode = getListOfFiles(dirToSrcCode);
		
		int i = 0;
		String cppSuffix = ".cpp";
		String javaSuffix = ".java";
		
		while (i < listOfSrcCode.length) {
			if(listOfSrcCode[i].getName().endsWith((cppSuffix)) || listOfSrcCode[i].getName().endsWith((javaSuffix))) {
				JCheckBox fileName = new JCheckBox(listOfSrcCode[i].getName());
				files.add(fileName);
				panel.add(fileName);
			}
			i++;
		}
		  
		return panel;
	}
	
	/**
	 * Method that takes path to directory, and returns a list of files.
	 * <p>
	 * 
	 * @param dir
	 * @return list
	 */
	public File[] getListOfFiles(File dir) {
		File[] list = dir.listFiles();
	
		return list;
	}
	/**
	 * Method to define the look and feel of GUI.
	 * <p>
	 * 
	 * @param none
	 * @return none
	 */
	private void setLookAndFeel() {
		try {
			UIManager.setLookAndFeel (
				"com.sun.java.swing.plaf.windows.WindowsLookAndFeel"
			);
		} catch (Exception exc) {
			//ignore error
		}
	}

	/**
	 * Method used to respond to user input.
	 * <p>
	 * 
	 * @param ActionEvent e
	 * @return none
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		String action = e.getActionCommand();
		
		if (action.equals("enterCodePath")) {
			File dirToSrcCode = new File(pathAddressToCode.getText());
			JPanel checkBoxRow = setCheckBox(dirToSrcCode);
			
			subPanelWithCards.add(checkBoxRow, "full");
			cl.show(subPanelWithCards, "full");
			
			System.err.println(dirToSrcCode.getPath());
		} else if (action.equals("genMut")) {
			
			for(int i = 0; i < files.size(); i++) {
				
				if(files.get(i).isSelected()) {
					
					File fileToSwap = new File(pathAddressToCode.getText() + files.get(i).getText());
					System.err.println("File to Mutate: " + fileToSwap.getPath());
					
					try {
						
						//Swap operator calls
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "+", "-", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "+", "/", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "+", "*", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "-", "+", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "-", "/", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "-", "*", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "<", "<=", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "<", ">", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "<", "==", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), ">", "<", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), ">", ">=", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), ">", "==", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "==", "<", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "==", ">", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "==", "<=", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "==", ">=", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "&&", "||", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "||", "&&", fileToSwap);
						MutationOperator.swapOperator(MutationOperator.LoadSourceFile(fileToSwap), "!", "", fileToSwap);
						
						//Swap if statements
						MutationOperator.swapIf(MutationOperator.LoadSourceFile(fileToSwap), fileToSwap);
						
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		} else if (action.equals("enterScriptPath")) {
			File dirToCompScript = new File(pathAddressToCompScript.getText());
			File[] list = getListOfFiles(dirToCompScript);
			int count = 0;
			
			while(count < list.length) {
				System.err.println("Compare Script: " + list[count].getName());
				
				count++;
			}
		} else {
			//else
		}

	}
	
}
