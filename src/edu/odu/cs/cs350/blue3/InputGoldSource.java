package edu.odu.cs.cs350.blue3;

import java.io.File;
import java.util.ArrayList;


/**
 * A class to collect the source code files for the Gold Version of the program 
 * 
 * 
 * Create interface
 * Design unit tests
 * Implement object/methods
 * test
 * commit
 * 
 * @author Matthew Breden
 *
 */
public class InputGoldSource
{
    
	
	/**
	 * Default constructor, no input or output.
	 * Does nothing, but we have a couple private data members
	 * that get initialized.
	 */
	public InputGoldSource()
	{
		
	}
	
	/**
	 * Non-default constructor
	 * Initializes other private fields to appropriate values based on input.
	 * No return.
	 * @param projectDir path (as a string) to  the desired project 
	 * directory. 
	 */
	public InputGoldSource(String projectDir)
	{
		projectDirName = projectDir;
		dirName = projectDirName + sourceDirName;
		sourceDir = new File(dirName);
		
	}
	
	/**
	 * Checks if the arraylist of files is empty
	 * @return arraylist.isEmpty();
	 */
	public boolean isEmpty()
	{
		return getSource().isEmpty();
	}
	
	/**
	 * Reads in files from source directory 
	 * specified at the top of the class and
	 * traverses subdirectories to get full
	 * paths of files found.
	 * Puts all files found into an arraylist
	 * Default with no parameter is project Directory
	 * specified on command line.
	 */
	
	 public void getFiles()
	 {
		 getFiles(dirName);
	 }
	 
	 public void getFiles(String path) {
		 //String path = dirName;

         File root = new File( path );
         File[] list = root.listFiles();

         if (list == null) return;

         for ( File f : list ) {
             if ( f.isDirectory() ) {
                 getFiles( f.getAbsolutePath() );
                 
                 //System.out.println( "Dir:" + f.getAbsoluteFile() );
             }
             else {
            	
 				if (f.isFile())
 				{
 					getSource().add(f.getAbsoluteFile());
 				}
                 System.out.println( "Filename: " + f.getAbsoluteFile() );
             }
         }
     }
	
	
	
	public ArrayList<File> getSource() {
		return source;
	}

	public void setSource(ArrayList<File> source) {
		this.source = source;
	}


	
	/** 
	 * All source files must be stored in the goldSource directory that resides in the root directory of this project
     * 
     */
	private String sourceDirName = "goldSource/";
	
	/**
	 * String combining project location with execution script location
	 * Should be initialized by constructor
	 */
	private String dirName;
	
	
	/**
	 * String listing the path to the project with subdirectories 
	 * containing source code.
	 * Default is "./sampleProject/"
	 */
	private String projectDirName = "./sampleProject/";
	
	private File sourceDir = new File(sourceDirName);
	private ArrayList<File> source = new java.util.ArrayList<File>();
}
