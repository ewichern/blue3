package edu.odu.cs.cs350.blue3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.odu.cs.cs350.blue3.InputGoldSource;
import edu.odu.cs.cs350.blue3.GetBuildFile;
import edu.odu.cs.cs350.blue3.ExecuteScripts;
import edu.odu.cs.cs350.blue3.CompareScripts;
// import edu.odu.cs.cs350.blue3.InputTestSuite;


/**
 * MutationDriver class for Mutation Testing Program
 * creates objects and executes methods to complete 
 * mutation testing. 
 * 
 * @author Erik Wichern , Anthony Baron , Tim Holmes - ewichern@cs.odu.edu , abaro007@odu.edu , tholmes@cs.odu.edu
 *
 */
public class MutationDriver extends MutationOperator {

	public static String projectDir = "./sampleProject";
	
	public static void main(String[] args) throws Exception {
		InputGoldSource goldSourceFiles = new InputGoldSource(projectDir);
		GetBuildFile buildFile = new GetBuildFile();
		ExecuteScripts testScripts = new ExecuteScripts();
		CompareScripts compareScripts = new CompareScripts();
		InputTestSuite testSuite = new InputTestSuite();
		
		File workingDir = new File ("./sampleProject/");
		ProcessBuilder p = new ProcessBuilder("make", "-f", "sampleProject/buildScript/makefile", "clean");
		p.directory(workingDir);
		ProcessBuilder q = new ProcessBuilder("make", "-f", "sampleProject/buildScript/makefile", "cleanTests");
		q.directory(workingDir);
		ProcessBuilder r = new ProcessBuilder("make", "-f", "sampleProject/buildScript/makefile");
		r.directory(workingDir);
		try {
			p.start();
		}
		catch (IOException e){
			System.err.println(e.getMessage());
		}
		try {
			q.start();
		}
		catch (IOException e){
			System.err.println(e.getMessage());
		}
		try {
			r.start();
		}
		catch (IOException e){
			System.err.println(e.getMessage());
		}
		
		
		
		goldSourceFiles.getFiles();
		source = goldSourceFiles.getSource();
		for (int i = 0; i < source.size(); i++){
			FileReader fileReader = new FileReader(source.get(i));
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			List<String> sourceStrings = new ArrayList<String>();
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				sourceStrings.add(line);
			}
			
				bufferedReader.close();
				File fileName = source.get(i);
				System.out.println(source.get(i));
				
				
				swapOperator(sourceStrings, "+", "-", fileName);
				swapOperator(sourceStrings, "+", "/", fileName);
				swapOperator(sourceStrings, "+", "*", fileName);
				swapOperator(sourceStrings, "-", "+", fileName);
				swapOperator(sourceStrings, "-", "/", fileName);
				swapOperator(sourceStrings, "-", "*", fileName);
				swapOperator(sourceStrings, "<", "<=", fileName);
				swapOperator(sourceStrings, "<", ">", fileName);
				swapOperator(sourceStrings, "<", "==", fileName);
				swapOperator(sourceStrings, ">", "<", fileName);
				swapOperator(sourceStrings, ">", ">=", fileName);
				swapOperator(sourceStrings, ">", "==", fileName);
				swapOperator(sourceStrings, "==", "<", fileName);
				swapOperator(sourceStrings, "==", ">", fileName);
				swapOperator(sourceStrings, "==", "<=", fileName);
				swapOperator(sourceStrings, "==", ">=", fileName);
				swapOperator(sourceStrings, "&&", "||", fileName);
				swapOperator(sourceStrings, "||", "&&", fileName);
				swapOperator(sourceStrings, "!", "", fileName);
				
				/**Change If(...) to If(true) and If(false)**/
				swapIf(sourceStrings, fileName);
				
		}
		
		
		
		assert (!goldSourceFiles.isEmpty());
		buildFile.getBuildFile();
		buildFile.echoBuildFile();
		testScripts.getFiles();
		assert (!testScripts.isEmpty());
		try {
			testScripts.runScripts();
		}
		catch (IOException e) {
			System.err.println("IO Exception at testScripts.runScripts(): " + e.getMessage());
		}
		compareScripts.getFiles();
		assert (!compareScripts.isEmpty());
		try {
			compareScripts.runScripts();
		}
		catch (IOException e) {
			System.err.println("IO Exception at compareScripts.runScripts(): " + e.getMessage());
		}
	
		File dirToTestData = new File("sampleProject" + File.separator + "testData" + File.separator);
		List<String> list = new ArrayList<String>();
		String suffix = ".dat";
		
		list = testSuite.DirectoryToList(dirToTestData, suffix);
		testSuite.PrintTestDataList(list);
		
		@SuppressWarnings("unused")
		MainInterface frame = new MainInterface();
	
	}
	private static ArrayList<File> source = new java.util.ArrayList<File>();
}