package edu.odu.cs.cs350.blue3;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * A class to input one or more CLI scripts that will run (already compiled) executables, 
 * execute tests, and capture the output of those tests
 * 
 * @author Erik Wichern - ewichern@cs.odu.edu
 *
 */
public class ExecuteScripts
{
	
	/**
	 * Default constructor
	 * Initializes other private fields to appropriate values based on default
	 * project directory - "./sampleProject/"
	 * No parameters or return.
	 */
	public ExecuteScripts()
	{
		dirName = projectDirName + scriptDirName;
		workingDir = new File(projectDirName);
		scriptDir = new File(dirName);
	}
		
	/**
	 * Non-default constructor
	 * Initializes other private fields to appropriate values based on input.
	 * No return.
	 * @param projectDir path (as a string) to desired the project 
	 * directory. 
	 */
	public ExecuteScripts(String projectDir)
	{
		dirName = projectDir + "/" + scriptDirName;
		workingDir = new File(projectDir);
		scriptDir = new File(dirName);
		
	}
	
	/**
	 * Checks if the arraylist of files is empty
	 * @return boolean = arraylist.isEmpty();
	 */
	public boolean isEmpty()
	{
		return scripts.isEmpty();
	}
	
	/**
	 * Reads in all executable files from script directory
	 * Either default directory or as specified as constructor parameter.
	 * Puts anything executable (checking for duplicates) into an arraylist.
	 * No input or output
	 * @throws IOException if there's a problem reading in one of the files 
	 * in the directory listing.
	 */
	public void getFiles() throws Exception
	{
		if (scriptDir.exists() && scriptDir.isDirectory())
		{
			for (File directoryListing : scriptDir.listFiles())
			{
				System.err.println("Filename: " + directoryListing.toString());
				if (directoryListing.isFile() && directoryListing.canExecute())
				{
					if (!scripts.contains(directoryListing))
							scripts.add(directoryListing);
				}
				System.err.println("Files in the array: "+ scripts.size());
			}
		}
	}
	
	/**
	 * Runs all files from Arraylist
	 * Using the ProcessBuilder library -- sets working directory (does not 
	 * work otherwise). getCanonicalPath() appears to be the better option 
	 * for cross-platform use, however I haven't done any windows testing.
	 * @throws IOException if there's a problem with one of the Files stored in
	 * the arraylist
	 */
	public void runScripts() throws Exception
	{
		for (File shellScript : scripts)
		{
			System.err.println("Script should run here.");
			System.err.println(shellScript.getCanonicalPath());
			ProcessBuilder pb = new ProcessBuilder(shellScript.getCanonicalPath());
			pb.directory(workingDir);
			pb.start();
		}
	}
	
	/**
	 * String listing the path to the project with subdirectories 
	 * containing scripts that describe test execution and output comparison.
	 * Default is "./sampleProject/"
	 */
	private String projectDirName = "./sampleProject/";
	/**
	 * String specifying the subdirectory of the project where script 
	 * describing test execution resides - "executeScripts/" 
	 */
	private String scriptDirName = "executeScripts/";
	/**
	 * String combining project location with execution script location
	 * Should be initialized by constructor
	 */
	private String dirName;
	/**
	 * File object to store the working directory for use with ProcessBuilder
	 */
	private File workingDir;
	/**
	 * File object to store the execution script directory for use with ProcessBuilder
	 */
	private File scriptDir;
	/**
	 * ArrayList container used to store File objects created from filename
	 * inputs.
	 */
	private ArrayList<File> scripts = new java.util.ArrayList<File>();
}
