package edu.odu.cs.cs350.blue3;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Class designed to a take a file directory that contains a test suite as input, and input the 
 * test suite into the program.
 *
 * Method for printing members of a test suite.
 * 
 * 
 * @author tholmes
 * @version 1.0
 * @since 2014-11-17
 *
 */

public class InputTestSuite {
	/**
	 * Method that takes a file directory and puts the content of the directory into a list.
	 * <p>
	 * 
	 * 
	 * @param Type: File, directory to test data.
	 * @return Type: File[], list of files in test data directory.
	 * 
	 */
	
	public List<String> DirectoryToList (File dirToTestData, String suffix) {
		
		File[] initialList = dirToTestData.listFiles();
		List<String> listTestData = new ArrayList<String>();
		
		for (File file : initialList) {
		    if (file.getName().endsWith((suffix))) {
		      listTestData.add(file.getName());
		    }
		  }
		
		return listTestData;
	}
	
	/**
	 * Prints list of test data in the test suite.
	 * <p>
	 * 
	 * @param Type: File, directory to test data.
	 * @return none
	 * 
	 */
	
	public void PrintTestDataList (List<String> testDataList) {
		int i= 0;
		
		System.out.println("Test Suite:");
		while(i < testDataList.size()) {
			System.out.println(i + ". " + testDataList.get(i)); 
			i++;
		}
	}
}
