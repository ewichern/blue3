package edu.odu.cs.cs350.blue3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MutationOperator {

	static int counter = 1;
	
	//MutationOper
	
	public static List<String> LoadSourceFile(File fileName) throws IOException {

		FileReader fileReader = new FileReader(fileName);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		List<String> sourceStrings = new ArrayList<String>();
		String line = null;
		while ((line = bufferedReader.readLine()) != null) {
			sourceStrings.add(line);
		}
		bufferedReader.close();
		return sourceStrings;
	}

	public static void createMutationFile(List<String> source, int counter,
			String find, File fileName) throws IOException {
		if (find == "/") {
			find = "DivSign";
		}
		if (find == "*") {
			find = "MultSign";
		}
		if (find == "<") {
			find = "lessThan";
		}
		if (find == ">") {
			find = "greaterThan";
		}
		if (find == "||") {
			find = "OR";
		}
		if (fileName.toString().contains(".cpp")) {
			File fileToWrite = new File("./sampleProject/mutations/" + counter
					+ "mutation" + find + ".cpp");
			fileToWrite.getParentFile().mkdirs();
			PrintWriter out = new PrintWriter(fileToWrite);
			for (int i = 0; i < source.size(); i++) {

				String item = source.get(i);
				out.println(item);
				++counter;
			}
			out.close();
		}
		if (fileName.toString().contains(".java")) {
			File fileToWrite = new File("./sampleProject/mutations/" + counter
					+ "mutation" + find + ".java");
			fileToWrite.getParentFile().mkdirs();
			PrintWriter out = new PrintWriter(fileToWrite);
			for (int i = 0; i < source.size(); i++) {

				String item = source.get(i);
				out.println(item);
				++counter;
			}
			out.close();
		}
	}

	public static void swapIf(List<String> source, File fileName)
			throws IOException {
		List<String> originalSource = new ArrayList<String>();
		originalSource = LoadSourceFile(fileName);
		String s = "";
		String newS = "";
		String newS2 = "";

		int localCounter = 0;

		List<Integer> indexSkip = new ArrayList<Integer>();
		for (int i = 0; i < source.size(); i++) {
			if (localCounter >= 15) {
				break;
			}
			else {
				s = source.get(i);
				if (s.contains("if")) {
					if (!indexSkip.contains(i)) {

						indexSkip.add(i);
						newS = s.replaceAll("\\(.*?\\)", "(true)");
						newS2 = s.replaceAll("\\(.*?\\)", "(false)");

						newS += " /** (...)" + " was replaced here **/";
						newS2 += " /** (...)" + " was replaced here **/";
						source.set(i, newS);

						createMutationFileIF(source, counter, fileName);
						++counter;
						localCounter += 1;
						source.set(i, newS2);
						createMutationFileIF(source, counter, fileName);
						++counter;
						i = 0;
						source = new ArrayList<String>(originalSource);
					}
				}	
			}
		}
	}

	public static void createMutationFileIF(List<String> source, int counter,
			File fileName) throws FileNotFoundException {

		if (fileName.toString().contains(".cpp")) {
			File fileToWrite = new File("./sampleProject/mutations/" + counter
					+ "mutationIF.cpp");
			fileToWrite.getParentFile().mkdirs();
			PrintWriter out = new PrintWriter(fileToWrite);
			for (int i = 0; i < source.size(); i++) {

				String item = source.get(i);
				out.println(item);
				++counter;
			}
			out.close();
		}
		if (fileName.toString().contains(".java")) {
			File fileToWrite = new File("./sampleProject/mutations/" + counter
					+ "mutationIF.java");
			fileToWrite.getParentFile().mkdirs();
			PrintWriter out = new PrintWriter(fileToWrite);
			for (int i = 0; i < source.size(); i++) {

				String item = source.get(i);
				out.println(item);
				++counter;
			}
			out.close();
		}

	}

	public static void swapOperator(List<String> source, String find,
			String replace, File fileName) throws IOException {
		List<String> originalSource = new ArrayList<String>();
		originalSource = LoadSourceFile(fileName);
		String s = "";
		String newS = "";
		int localCounter = 0;

		List<Integer> indexSkip = new ArrayList<Integer>();
		for (int i = 0; i < source.size(); i++) {
			if (localCounter < 15) {	
				s = source.get(i);

				if (s.contains(find)) {
					if (!indexSkip.contains(i)) {
						indexSkip.add(i);

						newS = s.replace(find, replace);
						newS += " /** " + find + " was replaced here **/";
						source.set(i, newS);
						createMutationFile(source, counter, find, fileName);
						++counter;
						localCounter += 1;
						i = 0;
						source = new ArrayList<String>(originalSource);
					}
				}
			}
			else
				break;
		}
	}

	public static void deleteStatement(List<String> source, File fileName)
			throws IOException {
		List<String> originalSource = new ArrayList<String>();
		originalSource = LoadSourceFile(fileName);
		String s = "";
		String newS = "";
		int localCounter = 0;

		List<Integer> indexSkip = new ArrayList<Integer>();
		for (int i = 0; i < source.size(); i++) {
			if (localCounter < 15) {

				s = source.get(i);

				if (!indexSkip.contains(i)) {
					indexSkip.add(i);
					s.trim();
				
					// newS = s.replaceAll("[^A-Za-z0-9]", "");
					// newS += "/** Statement was deleted here **/";
					// newS += "/** Statement-9 was deleted here **/";
					if (!(s.isEmpty())) {
						newS = "/** Statement on line " + (i+1) + " deleted here **/";
						source.set(i, newS);

						createMutationFileDELETE(source, counter, fileName);
						++counter;
						localCounter += 1;
					}
				
					i = 0;
					source = new ArrayList<String>(originalSource);
				}
			}
		}
	}

	public static void createMutationFileDELETE(List<String> source,
			int counter, File fileName) throws FileNotFoundException {

		if (fileName.toString().contains(".cpp")) {
			File fileToWrite = new File("./sampleProject/mutations/" + counter
					+ "mutationDS.cpp");
			fileToWrite.getParentFile().mkdirs();
			PrintWriter out = new PrintWriter(fileToWrite);
			for (int i = 0; i < source.size(); i++) {
				String item = source.get(i);
				out.println(item);
				++counter;
			}
			out.close();
		}
		if (fileName.toString().contains(".java")) {
			File fileToWrite = new File("./sampleProject/mutations/" + counter
					+ "mutationDS.java");
			fileToWrite.getParentFile().mkdirs();
			PrintWriter out = new PrintWriter(fileToWrite);
			for (int i = 0; i < source.size(); i++) {
				String item = source.get(i);
				out.println(item);
				++counter;
			}
			out.close();
		}
	}
	/**
	 * Had a main set up to test with
	 * 
	 * public static void main(String[] args) throws IOException { try { String
	 * fileName = "test.txt"; //LoadSourceFile(fileName);
	 * swapOperator(LoadSourceFile(fileName), "+", "-", fileName);
	 * swapOperator(LoadSourceFile(fileName), "-", "+", fileName);
	 * swapOperator(LoadSourceFile(fileName), "<", ">", fileName);
	 * swapOperator(LoadSourceFile(fileName), "/", "*", fileName); } catch
	 * (FileNotFoundException e) { e.printStackTrace(); }
	 * 
	 * }
	 **/
}
