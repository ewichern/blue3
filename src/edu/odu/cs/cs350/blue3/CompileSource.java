package edu.odu.cs.cs350.blue3;

import java.io.File;
import java.io.IOException;

public class CompileSource {

	CompileSource() {
		buildFile = new GetBuildFile();
		projectDir = new File(projectDirectory);
		buildFile.getBuildFile();
	}

	CompileSource(String inputDir) {
		buildFile = new GetBuildFile(inputDir);
		projectDir = new File(inputDir);
		buildFile.getBuildFile();
	}

	public void compile() throws Exception {
		
		System.err.println(buildFile.getName());
		
		if (buildFile.getName().equals("makefile")) {
			
			System.err.println(buildFile.getFile().getPath());
			System.err.println(projectDir.toString());
			
			File workingDir = new File(projectDir.toString());
			ProcessBuilder makeClean = new ProcessBuilder("make", "-f",
					buildFile.getFile().getCanonicalPath(), "clean");
			makeClean.directory(workingDir);
			makeClean.start();
			
			ProcessBuilder make = new ProcessBuilder("make", "-f",
					buildFile.getFile().getCanonicalPath());
			make.directory(workingDir);
			make.start();
		}
	}

	String projectDirectory = "./sampleProject";

	private File projectDir;
	private GetBuildFile buildFile;
}
